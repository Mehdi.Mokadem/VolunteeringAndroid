package com.example.mehdi.comment;

import java.io.Serializable;

/**
 * Created by Mehdi on 24/11/2017.
 */

public class Comment implements Serializable{
    int id;
    String comment;
    int idAtion;

    public Comment(int id, String comment, int idAtion) {
        this.id = id;
        this.comment = comment;
        this.idAtion = idAtion;
    }

    public int getIdAtion() {
        return idAtion;
    }

    public void setIdAtion(int idAtion) {
        this.idAtion = idAtion;
    }

    public Comment(int id, String comment) {
        this.id = id;
        this.comment = comment;
    }

    public Comment(String comment) {
        this.comment = comment;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "id=" + id +
                ", comment='" + comment + '\'' +
                '}';
    }
}