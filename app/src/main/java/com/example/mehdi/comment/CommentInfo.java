package com.example.mehdi.comment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.HashMap;


public class CommentInfo extends Fragment {

    EditText commentText;
    Button edit , delete;
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_comment_info, container, false);
        Bundle bundle = getArguments();
        final Comment comment = (Comment) bundle.getSerializable("comment");
        final  Post post = (Post)bundle.getSerializable("post");
        Log.i("qsd",comment.getComment());

        final RequestQueue mRequestQueue = Volley.newRequestQueue(getActivity());
        mRequestQueue.start();
        commentText = (EditText)view.findViewById(R.id.commentTextInfo);
        edit = (Button) view.findViewById(R.id.editCommentBtn);
        delete = (Button)view.findViewById(R.id.deleteCommentBtn);
        edit.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {

        final String URL = "http:/172.16.78.78:18080/volunteering-web/rest/test/"+comment.getId();
        HashMap<String, String> params = new HashMap<String, String>();

        params.put("comment", commentText.getText().toString());
        params.put("idA", String.valueOf(post.getId()));

        Log.i("jsonnn", (new JSONObject(params).toString()));


        final JsonObjectRequest request_json = new JsonObjectRequest(Request.Method.PUT,URL, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
commentText.setText(comment.getComment());

                        Toast.makeText(getActivity() , "Updated ",Toast.LENGTH_SHORT).show();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity() , "Updated ",Toast.LENGTH_SHORT).show();

                Log.i("edited", URL);

                error.getStackTrace();
            }
        });


// add the request object to the queue to be executed
        mRequestQueue.add(request_json);
        Log.i("mothod",String.valueOf(request_json.getMethod()));

    }
});

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = "http:/172.16.78.78:18080/volunteering-web/rest/test/"+comment.getId();
                StringRequest dr = new StringRequest(Request.Method.DELETE, url,
                        new Response.Listener<String>()
                        {
                            @Override
                            public void onResponse(String response) {
                               Toast.makeText(getActivity() , "Comment Deleted",Toast.LENGTH_SHORT).show();
                               CommentsFragment cmf =  new CommentsFragment();
                               Bundle  bundle = new Bundle();
                               bundle.putSerializable("post" , post);
                               cmf.setArguments(bundle);
                                getFragmentManager().beginTransaction().replace(R.id.container,cmf).addToBackStack(null).commit();

                            }

                        },
                        new Response.ErrorListener()
                        {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(getActivity() , "Comment already deleted",Toast.LENGTH_SHORT).show();

                            }
                        }
                );

                mRequestQueue.add(dr);
                Log.i("mothod",String.valueOf(dr.getMethod()));

            }
        });



        return view;
    }
}
