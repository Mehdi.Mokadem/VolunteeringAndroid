package com.example.mehdi.comment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 *
 */
public class CommentsFragment extends Fragment {

    ListView listComments ;
    ArrayList<Comment> comments;
    TextView test;
    FloatingActionButton addComment;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_comments, container, false);

        addComment = (FloatingActionButton)view.findViewById(R.id.addComment);
      Bundle bundle = getArguments();
        final Post  post =(Post) bundle.getSerializable("post");
addComment.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {

        Bundle bundle = new Bundle();
        addComment addCommentFrt = new addComment();
        bundle.putSerializable("post",post);
        addCommentFrt.setArguments(bundle);
        getFragmentManager().beginTransaction().replace(R.id.container,addCommentFrt).addToBackStack(null).commit();

    }
});



        listComments = (ListView)view.findViewById(R.id.commentList);


        comments= new ArrayList<>();
        RequestQueue mRequestQueue = Volley.newRequestQueue(getActivity());

        mRequestQueue.start();

        String url = "http:/172.16.78.78:18080/volunteering-web/rest/test/"+post.getId();
        Log.i("link",url);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println(response);
                        try {
                            JSONArray array = new JSONArray(response);
                            for (int i = 0; i < array.length(); i++){
                                JSONObject j = array.getJSONObject(i);
                                Comment p = new Comment(j.getInt("id"),j.getString("comment"));
                                comments.add(p);
                                Log.i("contrat" , p.toString());
                                Log.i("sdffff",comments.get(0).toString());
                                System.out.println(p);



                            }
                            listComments.setAdapter(new CommentCustomAdapter(getActivity(), R.layout.one_article, comments));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Handle error
                        System.out.println("Erreur "+error.getMessage());
                    }
                });

        mRequestQueue.add(stringRequest);

        listComments.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Comment comment = (Comment)adapterView.getItemAtPosition(i);

                Bundle bundle  = new Bundle();
                bundle.putSerializable("comment",comment);
                bundle.putSerializable("post",post);
                CommentInfo commentInfo = new CommentInfo();
                commentInfo.setArguments(bundle);
                getFragmentManager().beginTransaction().replace(R.id.container,commentInfo).addToBackStack(null).commit();


            }
        });
        comments.add(new Comment("dd"));
        return view;

    }


}