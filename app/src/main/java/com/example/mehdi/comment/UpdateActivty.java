package com.example.mehdi.comment;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.HashMap;

public class UpdateActivty extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_activty);


        RequestQueue mRequestQueue = Volley.newRequestQueue(this);

        mRequestQueue.start();
        String URL = "http:/172.16.78.78:18080/volunteering-web/rest/test/85";
        HashMap<String, String> params = new HashMap<String, String>();

        params.put("comment", "testetes321321t");

        Log.i("jsonnn", (new JSONObject(params).toString()));


        JsonObjectRequest request_json = new JsonObjectRequest(Request.Method.PUT,URL, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        //Process os success response
                     //   Log.i("tessdfsdft", "test");
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.getStackTrace();
                VolleyLog.e("Error123: ", error.getMessage());
            }
        });


// add the request object to the queue to be executed
        mRequestQueue.add(request_json);


    }
}
