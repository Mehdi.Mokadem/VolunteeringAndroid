package com.example.mehdi.comment;


import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class listPostFragment extends Fragment {   public  static  int rounds;
    ListView listPost ;
    ArrayList<Post> posts;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_post , container, false);



        listPost = (ListView)view.findViewById(R.id.postList);


        posts= new ArrayList<>();
        RequestQueue mRequestQueue = Volley.newRequestQueue(getActivity());

        mRequestQueue.start();

        String url = "http:/172.16.78.78:18080/volunteering-web/rest/test/act";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println(response);
                        try {

                            JSONArray array = new JSONArray(response);
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject j = array.getJSONObject(i);
                                Post p = new Post(j.getInt("id"),j.getString("name"),
                                        j.getString("description"));
                                posts.add(p);
                                Log.i("contrat" , p.toString());
                                Log.i("sdffff",posts.get(0).toString() );
                                System.out.println(p);



                            }
                            listPost.setAdapter(new PostCustomAdapter(getActivity(), R.layout.one_article, posts));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Handle error
                        System.out.println("Erreur "+error.getMessage());
                    }
                });

        mRequestQueue.add(stringRequest);
        //posts.add(new Post("dd","dd"));
        listPost.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Post post = (Post) adapterView.getItemAtPosition(i);
                Bundle bundle = new Bundle();
                CommentsFragment commentFRT = new CommentsFragment();
                bundle.putSerializable("post",post);
                commentFRT.setArguments(bundle);
                getFragmentManager().beginTransaction().replace(R.id.container,commentFRT).addToBackStack(null).commit();
            }
        });



        return view;
    }
}