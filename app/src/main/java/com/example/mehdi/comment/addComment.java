package com.example.mehdi.comment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.HashMap;


/**
 */
public class addComment extends Fragment {
    EditText comment ;
    Button addComment ;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_comment, container, false);
comment  = (EditText) view.findViewById(R.id.commentTextAdd);
addComment  = (Button)view.findViewById(R.id.addBtnAdd);
Bundle  bundle = getArguments();
final Post post  = (Post) bundle.getSerializable("post");


addComment.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {

        RequestQueue mRequestQueue = Volley.newRequestQueue(getActivity());

        mRequestQueue.start();

        String URL = "http:/172.16.78.78:18080/volunteering-web/rest/test/"+post.getId();
        HashMap<String, String> params = new HashMap<String, String>();

        params.put("comment", comment.getText().toString());

        Log.i("jsonnn", (new JSONObject(params).toString()));


        JsonObjectRequest request_json = new JsonObjectRequest(URL, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        //Process os success response
                        Log.i("tessdfsdft", "test");
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error123: ", error.getMessage());
            }
        });
        mRequestQueue.add(request_json);


// add the request object to the queue to be executed






    }
});

        return  view;
    }

}
