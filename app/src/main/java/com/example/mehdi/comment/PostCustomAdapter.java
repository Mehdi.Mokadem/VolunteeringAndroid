package com.example.mehdi.comment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class PostCustomAdapter extends ArrayAdapter<Post> {

	  private int resourceId = 0;
	  private LayoutInflater inflater;
	  public Context mContext;

	  public PostCustomAdapter(Context context, int resourceId, List<Post> mediaItems) {
	    super(context, 0, mediaItems);
	    this.resourceId = resourceId;
	    this.mContext = context;
	    inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	  }
	  
	  //ViewHolder Design Pattern
	  static class ViewHolder {
		    public TextView nameText, DescText;
		    public ImageView image;
		  }
	  
	  @Override
	  public View getView(int position, View convertView, ViewGroup parent) {
		  View rowView = convertView;
	  
		  //Réutiliser les Views
		  if (rowView == null) {
			rowView = inflater.inflate(resourceId, parent, false);
		  }
		  
		  //Configuration du ViewHolder
		  ViewHolder viewHolder = new ViewHolder();
		  viewHolder.nameText = (TextView) rowView.findViewById(R.id.nameItem);
		  viewHolder.DescText = (TextView) rowView.findViewById(R.id.commentItem);
		  rowView.setTag(viewHolder);
		  
		  //Affecter les données aux Views
		  ViewHolder holder = (ViewHolder) rowView.getTag();
		  Post post = getItem(position);
		  
		  holder.nameText.setText(post.getName());
		  holder.DescText.setText(post.getDescription());
		  notifyDataSetChanged();
		  return rowView;
	  }

	}



