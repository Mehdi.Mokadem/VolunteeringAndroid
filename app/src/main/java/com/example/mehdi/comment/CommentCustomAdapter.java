package com.example.mehdi.comment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;



import java.util.List;

public class CommentCustomAdapter extends ArrayAdapter<Comment> {

	  private int resourceId = 0;
	  private LayoutInflater inflater;
	  public Context mContext;

	  public CommentCustomAdapter(Context context, int resourceId, List<Comment> mediaItems) {
	    super(context, 0, mediaItems);
	    this.resourceId = resourceId;
	    this.mContext = context;
	    inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	  }

	  //ViewHolder Design Pattern
	  static class ViewHolder {
		    public TextView commentText;
		  }

	  @Override
	  public View getView(int position, View convertView, ViewGroup parent) {
		  View rowView = convertView;

		  //Réutiliser les Views
		  if (rowView == null) {
			rowView = inflater.inflate(resourceId, parent, false);
		  }

		  //Configuration du ViewHolder
		  ViewHolder viewHolder = new ViewHolder();
		  viewHolder.commentText = (TextView) rowView.findViewById(R.id.nameItem);
		  rowView.setTag(viewHolder);

		  //Affecter les données aux Views
		  ViewHolder holder = (ViewHolder) rowView.getTag();
		  Comment comment = getItem(position);

		  holder.commentText.setText(comment.getComment());
notifyDataSetChanged();
		  return rowView;
	  }

	}